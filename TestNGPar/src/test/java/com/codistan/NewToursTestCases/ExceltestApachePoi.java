package com.codistan.NewToursTestCases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ExceltestApachePoi {

	/*
	 * File Show Location FileInputStream Page Cell/Column and Row HSSFWorkBook -
	 * excel prior to 2007 with xls.
	 */

	@Test
	public void apachePoiSetup() throws IOException {

		File excelLocation = new File("C:\\Users\\Suleyman D\\eclipse-workspace\\TestNGPar\\DataDriven.xlsx");
		FileInputStream fis = new FileInputStream(excelLocation);
		// Obtains bytes from the File
		XSSFWorkbook wbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = wbook.getSheetAt(0);

		String data0 = sheet.getRow(0).getCell(0).getStringCellValue();
		String data1 = sheet.getRow(1).getCell(1).getStringCellValue();
//		System.out.println(data0);
//		System.out.println(data1);

		int rowCount = sheet.getLastRowNum();
		// System.out.println("Total rows :" + "" + rowCount);

		for (int i = 0; i < rowCount; i++) {
			String row0 = sheet.getRow(0).getCell(i).getStringCellValue();

			String row1 = sheet.getRow(2).getCell(i).getStringCellValue();
			// System.out.println(row0);
			System.out.println(row1);

		}
	}
}
