package com.codistan.NewToursTestCases;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.codistan.NewToursTestUtility.NewTourUtility;
import com.codistan.NewToursPageFactory.*;

public class NewTourTestCase {

	@Test
	public void NewTourTestCases() {

		NewTourUtility.getDriver();
		NewTourUtility.startApplication();
		NewToursLoginPage ntrs = new NewToursLoginPage();

		ntrs.logIn("tutorial", "tutorial");
	}

	@Test
	public void runTest() { // In order to run as a TestNG Test, We need to declare @Test Annotations

		String actualTitle = NewTourUtility.getDriver().getTitle();
		System.out.println(actualTitle);

		String expectedTitle = "Find a Flight: Mercury Tours:";

		Assert.assertEquals(actualTitle, expectedTitle);

	}
}
