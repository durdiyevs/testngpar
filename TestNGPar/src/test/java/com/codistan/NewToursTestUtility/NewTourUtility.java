package com.codistan.NewToursTestUtility;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import io.github.bonigarcia.wdm.WebDriverManager;

public class NewTourUtility {

	private static WebDriver driver = null;

	public Actions act;

	public NewTourUtility() {

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com"); // "http://newtours.demoaut.com"
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

	}

	public static ExtentReports report;

	public static ExtentTest test;

	public static void extentReportsStart() {

		String notDuplicate = new SimpleDateFormat("yyyyMMddhhmmss").format(new java.util.Date(0));

		String reportTarget = "C:\\Users\\Suleyman D\\eclipse-workspace\\TestNGPar" + "\\Extentreport" + notDuplicate
				+ ".html";
		report = new ExtentReports(reportTarget);
		test = report.startTest("Regression Suite-Avalon");
	}

	public static void extentreportsFlush() {
        report.endTest(test);
		report.flush();
		//Adds html report
	}

	public static String getScreenShot() throws IOException {
		String date = new SimpleDateFormat("MMMM dd, yy").format(new Date());

		TakesScreenshot ts = (TakesScreenshot) NewTourUtility.getDriver();

		File source = ts.getScreenshotAs(OutputType.FILE);

		String target = System.getProperty("user.dir") + ("test-output/NewToursScreenShots" + date + ".png"); // C:\Users\Suleyman
																												// D\eclipse-workspace\TestNGPar
		File finalDestination = new File(target);
		FileUtils.copyFile(source, finalDestination);
		return target;																										// same
																												// thing
																												// as
																												// user.di
		

	}
	public static String getScreenShot2() throws IOException   {
		String date = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		//Not to make my screenshot imgs dublicate
		//TakesScreenshot interface
		TakesScreenshot ts = (TakesScreenshot)NewTourUtility.getDriver();
		File source = ts.getScreenshotAs(OutputType.FILE);
		//C:\Users\Codistan\git\repository6\SeleniumBasicFrameworkTwo
		String target = System.getProperty("user.dir") + ("/test-output/NewToursScreenShots" + date +".png");
		File finalDestination = new File(target);
		FileUtils.copyFile(source, finalDestination);
		//It is not for flow of application
		return target;
	}
	public static WebDriver getDriver() {

		return driver;
	
	}

	public static void startApplication() {

		if (driver == null) {

			NewTourUtility amazonUtulity = new NewTourUtility();

		}
	}

	public static void closeApplication() {
		// TODO Auto-generated method stub
		return;
	}

}
