package com.codistan.testngdatadriven.dataprovider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DataProviderWithExcelInput {
	public static WebDriver driver;
	public static String url = "https://app.hellobonsai.com/users/sign_in";
	public int waitTime = 5;

	@BeforeMethod
	public void invokeChromeBrowser() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

	}

	@DataProvider(name = "ExcelData")
	public Object[][] readExcel() throws IOException {
		File excelL = new File(
				"C:\\Users\\Suleyman D\\eclipse-workspace\\TestNGPar\\ExcelFiles\\DataProviderExcel.xlsx");
		// File is not a mendatory step for excel reading

		FileInputStream fis = new FileInputStream(excelL);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet("Sheet1");

		int totalRaws = sheet.getLastRowNum();
		int totalColumns = sheet.getRow(0).getPhysicalNumberOfCells();

		Object[][] obj = new Object[totalRaws][totalColumns];

		for (int i = 0; i < totalRaws; i++) {

			obj[i][0] = sheet.getRow(0).getCell(0).getStringCellValue();
			System.out.println(obj[i][0] = sheet.getRow(0).getCell(0).getStringCellValue());
		}

		return obj;

	}
	@Test(dataProvider = "ExcelData")
	public void multipleInput(String email,String password) {
		driver.findElement(By.id("login-user-email")).sendKeys(email);
		driver.findElement(By.id("login-user-password")).sendKeys(password);
	}
	

}
