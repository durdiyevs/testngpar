package com.codistan.testngdatadriven.dataprovider;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestNGDataProvider {
	public WebDriver driver;

	/*
	 * @BeforeMethod
	 * 
	 * @DataProvider (name="TestData")
	 * 
	 * public Object [] [] () { Object [] [] data provider = new Object [3] [2]
	 * 
	 * @Test (dataProvider="TestData"
	 * 
	 * 
	 */

	@BeforeMethod
	public void openBrowser() {

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://app.hellobonsai.com/users/sign_in");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@DataProvider(name = "TestData")
	public Object[][] getData() {

		Object[][] data = new Object[4][2];  //First one is how many times / second one is 2 value
		data[0][1] = "123456";
		data[0][0] = "user1@gmail.com";

		

		data[1][0] = "user2@gmail.com";

		data[1][1] = "1234567";

		data[2][0] = "user3@gmail.com";

		data[2][1] = "12345678";

		return data;
	}

	@Test(dataProvider = "TestData")
	public void loginBonsai(String email, String password) {

		System.out.println(email);
		// System.out.println(password);

		driver.findElement(By.id("login-user-email")).sendKeys(email);
		driver.findElement(By.id("login-user-password")).sendKeys(password);

	}
	
	@AfterMethod
	public void closeDown() {
		
	}

}
