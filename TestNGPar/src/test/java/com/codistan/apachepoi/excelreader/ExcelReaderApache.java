package com.codistan.apachepoi.excelreader;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReaderApache {
	XSSFWorkbook wbook;
	XSSFSheet sheet;
	
	public ExcelReaderApache(String path) {
		
	try {
	File excelLocation = new File(path);  //"C:\\Users\\Suleyman D\\eclipse-workspace\\TestNGPar\\DataDriven.xlsx"
	FileInputStream fis = new FileInputStream(excelLocation);
	//Obtains bytes from the File
	 wbook = new XSSFWorkbook(fis);
	 sheet = wbook.getSheetAt(0);
	}catch (Exception e) {
System.out.println(e.getMessage());
	}
	
}
	
	public String getData(int sheetNumber, int row, int cell) {
		sheet = wbook.getSheetAt(sheetNumber);
		String getData =sheet.getRow(row).getCell(cell).getStringCellValue();
		 
		return getData;

	
	}
}
