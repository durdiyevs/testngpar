package com.codistan.NewToursPageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.codistan.NewToursTestUtility.NewTourUtility;

public class NewTourrRegistryPage {

	 public NewTourrRegistryPage() {
PageFactory.initElements(NewTourUtility.getDriver(), this);
	}
	
	@FindBy (xpath = "//a[contains( text(), 'REGISTER')]") 
	@CacheLookup      //to optimize the 
	public WebElement registerButton;    
	
	@FindBy (xpath ="//select[@name='country']")
	public WebElement countryDropDown;
	
	public void selectCountry(String country) {
		registerButton.click();
		Select slct = new Select (countryDropDown);
		slct.selectByVisibleText(country);
		
	}
	
}
