 package com.codistan.NewToursPageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Parameters;

import com.codistan.NewToursTestUtility.*;

public class NewToursLoginPage {
	
	public static void main(String[] args) {

	}

	public NewToursLoginPage() {

		PageFactory.initElements(NewTourUtility.getDriver(), this);

	}

	@FindBy(xpath = "//input[@name='userName']")
	public WebElement userName;

	@FindBy(xpath = "//input[@name='password']")
	public WebElement passWord;

	@FindBy(xpath = "//input[@name='login']")
	public WebElement clickButton;
    
	@Parameters({"userN","passW"})
	public void logIn(String userN, String passW) {

		userName.sendKeys(userN);
		passWord.sendKeys(passW);
		clickButton.click();
	}

}
