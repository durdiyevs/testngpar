package com.codistan.NewToursPageFactory;

import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.codistan.NewToursTestUtility.NewTourUtility;

public class NewToursTestBase {

@BeforeTest
// 1 time

public void beforeTest()  {
	//Test
	NewTourUtility.startApplication();
	NewTourUtility.extentReportsStart();
	
	
}
@AfterTest
public void afterTest() throws IOException {
	
	NewTourUtility.getScreenShot();
	NewTourUtility.extentreportsFlush();
	
}
	
}
